using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.ServiceBus.Messaging;

namespace wfefunctionapp
{
    //this is just here to test things out a bit.
    public static class Something
    {
        public static string something()
        {
            return "something";
        }
    }
    public static class SBTest
    {
        [FunctionName("SBTest")]
        public static void Run([ServiceBusTrigger("incomingbills", AccessRights.Manage, Connection = "ServiceBus")]string myQueueItem, TraceWriter log)
        {
            log.Info($"C# ServiceBus queue trigger function processed message: {myQueueItem} " + Something.something());
        }
    }
}
